import numpy as np
import matplotlib . pyplot as plt

data = np.loadtxt("data.csv", delimiter=",", dtype=float, skiprows=1)

#A
print("Mjerenja su izvršena na", data.shape[0], "ljudi.")

#B
height=data[:,1]
weight=data[:,2]

plt.scatter(height,weight)
plt . xlabel ('x-os')
plt . ylabel ('y-os')
plt . title ( 'Primjer')
plt . show ()

#C
height_50 = data[:,1][0::50]
weight_50 = data[:,2][0::50]

plt.title(" Omjer visine i tezine za svaku 50u osobu")
plt.xlabel("Visina")
plt.ylabel("Tezina")
plt.scatter(height_50, weight_50, color="g")
plt.show()

#D
print("Minimalna visina:"+str(np.min(height)))
print("Maksimalna visina"+str(np.max(height)))
print("Prosjecna visina:"+str(np.mean(height)))


#E
men = data[np.where(data[:,0] == 1)]
female = data[np.where(data[:,0] == 0)]
men = men[:,1]
female = female[:,1]

print(" Najmanja visina muskaraca je", min(men), "cm, a najveca", max(men), "cm.")
print("Prosječna muska visina", men.mean(), "cm.")
print("Najmanja visina u ženskom skupu je", min(female), "cm, a najveća", max(female), "cm.")
print("Prosječna zenska visina", female.mean(), "cm.")