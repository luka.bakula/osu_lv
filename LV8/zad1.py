import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix


# Load the saved model from Task 1
model = keras.models.load_model('my_model.h5')

# Load the MNIST test data
(_, _), (x_test, y_test) = keras.datasets.mnist.load_data()

# Normalize the pixel values to [0, 1]
x_test = x_test.astype('float32') / 255.0

# Reshape the images to (28, 28, 1)
x_test = np.expand_dims(x_test, axis=-1)

# Make predictions on the test set
y_pred = model.predict(x_test)

# Get the predicted labels for each image
y_pred_labels = np.argmax(y_pred, axis=1)

# Get the true labels for each image
y_true = y_test

# Find the misclassified images
misclassified_idx = np.where(y_pred_labels != y_true)[0]

# Display several misclassified images with their predicted and true labels
num_images = 5
fig, axes = plt.subplots(nrows=1, ncols=num_images, figsize=(10, 5))

for i in range(num_images):
    idx = misclassified_idx[i]
    img = x_test[idx]
    img_label = y_true[idx]
    pred_label = y_pred_labels[idx]
    axes[i].imshow(img.squeeze(), cmap='gray')
    axes[i].set_title(f"True: {img_label}\nPred: {pred_label}")
    axes[i].axis('off')

plt.show()
