import pandas as pd
data = pd.read_csv('data_C02_emission.csv')
# a)
print(f'Dataframe sadrzi:{len(data)} mjerenja')
print(data.dtypes)
print(data.isna().sum())
print(data.duplicated().sum())
data.dropna()
data.drop_duplicates()

#data['vehicle_class'] = data['vehicle_class'].astype('category')

cols = ['Make', 'Model', 'Vehicle Class', 'Transmission', 'Fuel Type']
data[cols] = data[cols].astype('category')
print(data.dtypes)
# print(data.info())

# b)

consumptionMax = data[['Make', 'Model', 'Fuel Consumption City (L/100km)']].nlargest(
    3, ['Fuel Consumption City (L/100km)'])
print(consumptionMax)

consumptionMin = data[['Make', 'Model', 'Fuel Consumption City (L/100km)']].nsmallest(
    3, ['Fuel Consumption City (L/100km)'])
print(consumptionMin)

# C)

num_vehicles = len(data[(data['Engine Size (L)'] > 2.5)
                   & (data['Engine Size (L)'] < 3.5)])
avg_co2 = data[(data['Engine Size (L)'] > 2.5) & (
    data['Engine Size (L)'] < 3.5)]['CO2 Emissions (g/km)'].mean()

print(num_vehicles)
print(avg_co2)

# D)

audiDrivers = len(data[(data)['Make'] == 'Audi'])
print(audiDrivers)
avg_co2_emissions_audi = data[(data['Make'] == 'Audi') & (
    data['Cylinders'] == 4)]['CO2 Emissions (g/km)'].mean()
print(avg_co2_emissions_audi)

# E)

evenCylinders = data[data['Cylinders'] % 2 == 0]
avg_co2_emissions_by_cylinders = evenCylinders.groupby(
    'Cylinders')['CO2 Emissions (g/km)'].mean()
print(evenCylinders.count())
print(avg_co2_emissions_by_cylinders)

# F)
