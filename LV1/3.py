numbers = []
while True:
    user_input = input("Unesi broj sve do Done ")
    if user_input == 'Done':
        break
    else:
        try:
            number = float(user_input)
            numbers.append(number)
        except ValueError:
            print("Netocan input,mora biti broj!")

if numbers:
    count = len(numbers)
    average = sum(numbers) / count
    minimum = min(numbers)
    maximum = max(numbers)
    numbers.sort()

    print(f"Lista ima {count} clanova")
    print(f" Prosjek:{average}")
    print(f"Najmanji:{minimum}.")
    print(f"Najveci:{maximum}.")
    print("Sortirana lista:")
    for number in numbers:
        print(number)
else:
    print("Nisi unio broj")