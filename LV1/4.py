count=0
word_freq = {}
with open('song.txt', 'r') as file:
    for line in file:
        words = line.strip().split()
        for word in words:
            if word not in word_freq:
                word_freq[word] = 1
            else:
                word_freq[word] += 1

# Print the words that appear only once
print("Words that appear only once:")
for word, freq in word_freq.items():
    if freq == 1:
        print(word)
        count=count+1

print(count)