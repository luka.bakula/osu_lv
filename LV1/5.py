ham_count = 0
ham_total_words = 0
spam_count = 0
spam_total_words = 0
spam_exclamation_count = 0

with open('SMSSpamCollection.txt', 'r') as file:
    for line in file:
        line = line.strip()
        if line.startswith('ham'):
            ham_count += 1
            ham_total_words += len(line.split()[1:])
        elif line.startswith('spam'):
            spam_count += 1
            spam_total_words += len(line.split()[1:])
            if line.endswith('!'):
                spam_exclamation_count += 1

ham_avg_words = ham_total_words / ham_count
spam_avg_words = spam_total_words / spam_count

print(f"Average number of words in ham messages: {ham_avg_words:.2f}")
print(f"Average number of words in spam messages: {spam_avg_words:.2f}")
print(f"Number of spam messages that end with an exclamation mark: {spam_exclamation_count}")
