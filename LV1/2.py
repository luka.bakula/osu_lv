try:
    grade=float(input())
    while grade<0 or grade>1:
      grade=float(input())
    if grade<0.6 and grade >= 0.0:
      print("F")
    if(grade>=0.6 and grade<0.7):
      print("D")
    if(grade>=0.7 and grade<0.8):
      print("C")
    if(grade>=0.8 and grade<0.9):
      print("B")
    if(grade>=0.9 and grade<=1):
      print("A")    
    
except:
 print("Wrong input")
 